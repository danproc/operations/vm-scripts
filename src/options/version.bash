#!/usr/bin/env bash

# display current version
function executeOptionVersion {
  grep -m 1 "^## "  "${srcDir}/CHANGELOG.md" | cut -d ' ' -f 2
}
