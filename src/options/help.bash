#!/usr/env/bash

# display help menu
function executeOptionHelp {
  echo
  echo "Usage:"
  echo "  ${cmd} <command> [<options>]"
  echo
  echo "Manage virtual machines via cloud-init and KVM."
  echo
  echo "Commands:"
  echo "  list           list all existing virtual machines"
  echo "  pull           pull latest cloud image"
  echo "  create         create new virtual machine"
  echo "  delete         delete existing virtual machine"
  echo "  update         update this script from git repository"
  echo "    -f, --force  force update by purging local repository"
  echo
  echo "Options:"
  echo "  -h, --help     display this help and exit"
  echo "  -v, --version  display the current version"
  echo
}
