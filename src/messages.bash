#!/usr/bin/env bash

# message indicators
info="${g}info:${d}   "
warn="${y}warn:${d}   "
error="${r}error:${d}  "
