#!/usr/bin/env bash

# display that function is not implemented
function notImplemented {
  echo -e "${warn}This command is not yet implemented."
  exit 1
}