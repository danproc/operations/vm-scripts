#!/usr/bin/env bash

# display that a command was used incorrectly and suggest help command
function displayUsageError {
  echo -e "${error}This command or option is not supported."
  echo -e "${error}Try running: ${m}vms -h${d}"
}
