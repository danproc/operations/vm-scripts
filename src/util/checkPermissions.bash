#!/usr/bin/env bash

# check if the effective user id is the id of root
function checkPermissions {
  if [[ $EUID -ne 0 ]]; then
    echo -e "${error}This script must be run as ${m}root${d} or with ${m}sudo${d}."
    exit 1
  fi
}
