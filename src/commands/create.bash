#!/usr/bin/env bash

# create new virtual machine
function executeCommandCreate {
  source "${srcDir}/src/util/checkPermissions.bash"
  source "${srcDir}/src/commands/pull.bash"

  checkPermissions
  executeCommandPull

  local image="${ubuntuXenialImage}"
  local osVariant="${ubuntuXenialOsVariant}"

  # get machine name
  local vmNameDefault=""
  local vmName=""
  local returnCode=""

  function generateUniqueVmName {
    vmNameDefault="vm-$(openssl rand -hex 2)"
    virsh list --all | grep "${vmNameDefault}" > /dev/null
    returnCode=$?
  }

  function scanVmName {
    echo -en "${info}What should the name of the new VM be? [${vmNameDefault}] "
    read vmName
    virsh list --all | grep "${vmName}" > /dev/null
    returnCode=$?
  }

  generateUniqueVmName

  while [ ${returnCode} -eq 0 ]; do
    generateUniqueVmName
  done

  scanVmName

  while [ -n "${vmName}" ] && [ ${returnCode} -eq 0 ]; do
    echo -e "${warn}A machine with this name already exists. Please choose a different name."
    scanVmName
  done

  if [ -z "${vmName}" ]; then
    vmName="${vmNameDefault}"
  fi

  # get CPU count
  local vmCpuDefault="1"
  local vmCpu=""

  echo -en "${info}How many virtual cores should the VM have? [${vmCpuDefault}] "
  read vmCpu

  if [ -z "${vmCpu}" ]; then
    vmCpu="${vmCpuDefault}"
  fi

  # get RAM size
  local vmRamDefault="512"
  local vmRam=""

  echo -en "${info}How much memory should the VM have? [${vmRamDefault}] "
  read vmRam

  if [ -z "${vmRam}" ]; then
    vmRam="${vmRamDefault}"
  fi

  # get root filesystem size
  local vmRootDefault="10"
  local vmRoot=""

  echo -en "${info}How many gigabytes big should the root filesystem be? [${vmRootDefault}] "
  read vmRoot

  if [ -z "${vmRoot}" ]; then
    vmRoot="${vmRootDefault}"
  fi

  # get network gateway
  local vmNetGatewayDefault="10.0.0.1"
  local vmNetGateway=""

  echo -en "${info}What is the network gateway? [${vmNetGatewayDefault}] "
  read vmNetGateway

  if [ -z "${vmNetGateway}" ]; then
    vmNetGateway="${vmNetGatewayDefault}"
  fi

  # get network mask
  local vmNetMaskDefault="255.255.255.0"
  local vmNetMask=""

  echo -en "${info}What is the network mask? [${vmNetMaskDefault}] "
  read vmNetMask

  if [ -z "${vmNetMask}" ]; then
    vmNetMask="${vmNetMaskDefault}"
  fi

  # get network address
  local vmNetAddressDefault="10.0.0.2"
  local vmNetAddress=""

  echo -en "${info}What should the IP of the VM be? [${vmNetAddressDefault}] "
  read vmNetAddress

  if [ -z "${vmNetAddress}" ]; then
    vmNetAddress="${vmNetAddressDefault}"
  fi

  # get ssh public key
  local vmPubKeyPathDefault="${HOME}/.ssh/id_rsa.pub"
  local vmPubKeyPath=""

  echo -en "${info}Which public key should be used? [${vmPubKeyPathDefault}] "
  read vmPubKeyPath

  if [ -z "${vmPubKeyPath}" ]; then
    vmPubKeyPath="${vmPubKeyPathDefault}"
  fi

  local vmPubKey=$(cat "${vmPubKeyPath}")

  # get vm template
  local vmProvisioningDefault="none"
  local vmProvisioning

  echo -en "${info}Which provisioning script should be used? [${vmProvisioningDefault}] "
  read vmProvisioning

  if [ -z "${vmProvisioning}" ]; then
    vmProvisioning="${vmProvisioningDefault}"
  fi

  export vmNetGateway vmNetMask vmNetAddress vmPubKey vmName

  echo -e "${info}Creating metadata files ..."
  mkdir -p "${metaDir}/${vmName}"
  awk '{while(match($0,"[$]{[^}]*}")) {var=substr($0,RSTART+2,RLENGTH -3);gsub("[$]{"var"}",ENVIRON[var])}}1' < "${srcDir}/src/templates/network-config.yaml" > "${metaDir}/${vmName}/network-config.yaml"
  awk '{while(match($0,"[$]{[^}]*}")) {var=substr($0,RSTART+2,RLENGTH -3);gsub("[$]{"var"}",ENVIRON[var])}}1' < "${srcDir}/src/templates/user-data.yaml" > "${metaDir}/${vmName}/user-data.yaml"

  echo -e "${info}Preparing provisioning script: ${m}${vmProvisioning}${d}"
  case "${vmProvisioning}" in
    "haproxy_1.8")
      cat "${srcDir}/src/templates/${vmProvisioning}.yaml" >> "${metaDir}/${vmName}/user-data.yaml"
      ;;
    "docker-ce_17.03.2")
      cat "${srcDir}/src/templates/${vmProvisioning}.yaml" >> "${metaDir}/${vmName}/user-data.yaml"
      ;;
    "kubernetes_1.11")
      cat "${srcDir}/src/templates/docker-ce_17.03.2.yaml" >> "${metaDir}/${vmName}/user-data.yaml"
      cat "${srcDir}/src/templates/${vmProvisioning}.yaml" >> "${metaDir}/${vmName}/user-data.yaml"
      ;;
    *)
      cat "${srcDir}/src/templates/none.yaml" >> "${metaDir}/${vmName}/user-data.yaml"
  esac

  echo -e "${info}Creating machine root filesystem disk ..."
  cp "${imageDir}/${image}.img" "${libvirtImageDir}/${vmName}.img" > /dev/null

  echo -e "${info}Resizing machine root filesystem disk ..."
  qemu-img resize "${libvirtImageDir}/${vmName}.img" "${vmRoot}G" > /dev/null

  echo -e "${info}Creating machine configuration filesystem disk ..."
  cloud-localds -N "${metaDir}/${vmName}/network-config.yaml" "${metaDir}/${vmName}/config.img" "${metaDir}/${vmName}/user-data.yaml"

  echo -e "${info}Starting virtual machine ..."
  virt-install \
    --name "${vmName}" \
    --vcpus "${vmCpu}" \
    --ram "${vmRam}" \
    --disk "${libvirtImageDir}/${vmName}.img,device=disk,bus=virtio,size=${vmRoot}" \
    --disk "${metaDir}/${vmName}/config.img,device=cdrom" \
    --os-type linux \
    --os-variant "${osVariant}" \
    --network bridge:br0,model=virtio \
    --graphics none \
    --console pty,target_type=serial \
    --noautoconsole > /dev/null

  echo -e "${info}Removing configuration filesystem disk ..."
  virsh change-media "${vmName}" hda --eject --config > /dev/null

  echo -e "${info}Marking machine for autostart ..."
  virsh autostart "${vmName}" > /dev/null

  echo -e "${info}Removing old machine host keys ..."
  sudo -u ubuntu ssh-keygen -f "${HOME}/.ssh/known_hosts" -R "${vmNetAddress}" &> /dev/null

  echo -e "${info}The virtual machine has successfully been created!"
}
