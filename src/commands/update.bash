#!/usr/bin/env bash

# update this script from git repository
function executeCommandUpdate {
  source "${srcDir}/src/util/checkPermissions.bash"
  source "${srcDir}/src/util/displayUsageError.bash"

  checkPermissions

  # change to source directory
  function changeDir {
    pushd "${srcDir}" > /dev/null
  }

  # pull from git repository
  function pullRepo {
    echo -e "${info}Pulling latest version ..."
    git pull origin master

    if [ $? -ne 0 ]; then
      echo -e "${error}Updating from remote repository failed!"
      echo -e "${error}Try running: ${m}vms update -f${d}"
      popd > /dev/null
      exit 1
    fi

    echo -e "${info}The program has successfully been updated!"
    popd > /dev/null
  }

  # purge repository
  function purgeRepo {
    echo -e "${warn}Purging local repository ..."
    git reset --hard > /dev/null
    git clean -fd > /dev/null
  }

  case "${2}" in
    "-f"|"--force")
      changeDir
      purgeRepo
      pullRepo
      ;;
    "")
      changeDir
      pullRepo
      ;;
    *)
      displayUsageError
      ;;
  esac
}
