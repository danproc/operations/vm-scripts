#!/usr/bin/env bash

# list existing virtual machines
function executeCommandList {
  source "${srcDir}/src/util/checkPermissions.bash"

  checkPermissions

  virsh list --all
}
