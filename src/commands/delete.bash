#!/usr/bin/env bash

# delete existing virtual machine
function executeCommandDelete {
  source "${srcDir}/src/util/checkPermissions.bash"

  checkPermissions

  # get machine name
  local vmName=""
  local returnCode=""

  function scanVmName {
    echo -en "${info}Which VM do you want to delete? "
    read vmName
    virsh list --all | grep "${vmName}" > /dev/null
    returnCode=$?
  }

  scanVmName

  while [ -n "${vmName}" ] && [ ${returnCode} -eq 1 ]; do
    echo -e "${warn}A machine with this name does not exist."
    echo -e "${warn}Please choose a different one or type: ${m}vms list${d}"
    scanVmName
  done

  if [ -z "${vmName}" ]; then
    echo -e "${warn}No machine name was supplied."
    echo -e "${warn}Aborting deletition."
    exit 1
  fi

  echo -e "${info}Shutting the machine down ..."
  virsh destroy "${vmName}" > /dev/null

  echo -e "${info}Undefining machine ..."
  virsh undefine "${vmName}" > /dev/null

  echo -e "${info}Deleting metadata files ..."
  rm -rf "${metaDir}/${vmName}"

  echo -e "${info}Deleting root filesystem image ..."
  rm "${libvirtImageDir}/${vmName}.img"

  echo -e "${info}Successfully removed virtual machine ${m}${vmName}${d}!"
}
