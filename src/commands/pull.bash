#!/usr/bin/env bash

# pull latest cloud image
function executeCommandPull {
  source "${srcDir}/src/util/checkPermissions.bash"

  local mirror="${ubuntuXenialMirror}"
  local image="${ubuntuXenialImage}"
  local checksums="${ubuntuXenialChecksums}"

  # fetch and generate checksums
  function generateChecksums {
    # checksums need to be generated from within the directory of the file
    # otherwise there will be a checksum mismatch if done from another location
    # therefore the current workdir is changed temporarily
    pushd "${imageDir}" > /dev/null
    local latestChecksum=$(wget -qO- "${mirror}/${checksums}" | grep "${image}.img" | awk '{ print $1 }')
    local localChecksum=$(sha256sum -b "${imageDir}/${image}.img" | awk '{ print $1 }')
    popd > /dev/null
  }

  # download cloud image
  function downloadImage {
    wget "${mirror}/${image}.img" -qO "${imageDir}/${image}.img" --show-progress
  }

  # ensure image directory exsists
  if [ ! -d "${imageDir}" ]; then
    echo -e "${info}Creating image directory ${m}${imageDir}${d} ..."
    mkdir -p "${imageDir}" > /dev/null
  fi

  echo -e "${info}Checking local image ..."

  # download image if not present on disk
  if [ ! -f "${imageDir}/${image}.img" ]; then
    echo -e "${info}Downloading image ${m}${image}.img${d} ..."
    downloadImage
  fi

  generateChecksums

  # verify checksums of downloaded image and retry on mismatch
  while [ "${localChecksum}" != "${latestChecksum}" ]; do
    echo -e "${warn}Checksum mismatch!"
    echo -e "${warn}Latest: ${latestChecksum}"
    echo -e "${warn}Local: ${localChecksum}"
    echo -e "${info}Fetching latest image ..."
    downloadImage

    generateChecksums
  done

  echo -e "${info}The image is up-to-date."
}
