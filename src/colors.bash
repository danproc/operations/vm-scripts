#!/usr/bin/env bash

# color variables
default='\e[00;0m'
red='\e[00;31m'
green='\e[00;32m'
yellow='\e[00;33m'
magenta='\e[00;35m'

# color shortcuts
d=${default}
r=${red}
g=${green}
y=${yellow}
m=${magenta}
