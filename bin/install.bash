#!/usr/bin/env bash

# setup variables
appName="vm-scripts"
srcBaseDir="/usr/src"
srcDir="${srcBaseDir}/${appName}"
dataBaseDir="/var/lib"
dataDir="${dataBaseDir}/${appName}"
cmd="vms"
repoUrl="https://gitlab.com/danproc/operations/vm-scripts.git"

# color variables
default='\e[00;0m'
red='\e[00;31m'
green='\e[00;32m'
magenta='\e[00;35m'

# color shortcuts
d=${default}
r=${red}
g=${green}
m=${magenta}

# message indicators
info="${g}info:${d}   "
error="${r}error:${d}  "

# runtime variables
missingDependencies=()

# check user permissions
function checkPermissions {
  if [[ ${EUID} -ne 0 ]]; then
    echo -e "${error}This script must be run as ${m}root${d} or with ${m}sudo${d}."
    exit 1
  fi
}

# check if script is piped to bash
function checkPipe {
  if [ ! -t 0 ]; then
    echo -e "${error}This script can't be piped into bash."
    echo -e "${error}Please download the script and execute it afterwards."
    echo -e "${error}For further information refer to: ${m}https://gitlab.com/danproc/vm-scripts/${d}."
    exit 1
  fi
}

# join array with supplied delimiter
function joinArrayBy {
  local delimiter="${1}"
  shift
  echo -n "${1}"
  shift
  printf "%s" "${@/#/$delimiter}"
}

# update local package index
function updatePackageList {
  echo -e "${info}Updating package list ..."
  apt-get update -y
}

# install package if not alread installed
function selectPackage {
  echo -e "${info}Checking if ${m}${1}${d} is installed ..."

  if [ $(dpkg-query -W -f='${Status}' ${1} 2> /dev/null | grep -c "ok installed") -eq 0 ]; then
    echo -e "${info}Marking ${m}${1}${d} for installation ..."
    missingDependencies+=("${1}")
  fi
}

# install required dependencies
function installDependencies {
  echo -e "${info}Installing dependencies ..."
  apt-get install $(joinArrayBy ' ' "${missingDependencies[@]}") -y --no-install-recommends
}

# install application
function installApplication {
  echo -e "${info}Preparing source directory ..."

  if [ ! -d "${srcBaseDir}" ]; then
    echo -e "${info}Creating directory ${m}${srcBaseDir}${d} ..."
    mkdir -p "${srcBaseDir}" > /dev/null
  fi

  if [ -d "${srcDir}" ]; then
    echo -e "${error}The application is already installed. Please uninstall first before reinstalling."
    exit 1
  fi

  echo -e "${info}Preparing data directory ..."

  if [ ! -d "${dataDir}" ]; then
    echo -e "${info}Creating directory ${m}${dataDir}${d} ..."
    mkdir -p "${dataDir}" > /dev/null
  else
    echo -e "${info}Purging data directory ${m}${dataDir}${d} ..."
    rm -rf "${dataDir}/*" > /dev/null
  fi

  echo -e "${info}Cloning repository to ${m}${srcDir}${d} ..."
  git clone "${repoUrl}" "${srcDir}" > /dev/null

  echo -e "${info}Adjusting permissions ..."
  chmod +x "${srcDir}/bin/vms.bash"

  echo -e "${info}Linking ${m}/bin/${cmd}${d} ..."
  ln -s "${srcDir}/bin/vms.bash" "/bin/${cmd}" > /dev/null
}

# display that installation has succeeded
function displaySuccess {
  echo -e "${info}The application has successfully been installed!"
  echo -e "${info}To display the help menu, type: ${m}vms -h${d}"
  exit 0
}

checkPipe
checkPermissions

updatePackageList

selectPackage git

installDependencies

installApplication

selectPackage qemu-kvm
selectPackage libvirt-bin
selectPackage virtinst
selectPackage bridge-utils
selectPackage cloud-image-utils

installDependencies

displaySuccess
