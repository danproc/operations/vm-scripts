#!/usr/bin/env bash

# setup variables
appName="vm-scripts"
srcBaseDir="/usr/src"
srcDir="${srcBaseDir}/${appName}"
dataBaseDir="/var/lib"
dataDir="${dataBaseDir}/${appName}"
cmd="vms"

# color variables
default='\e[00;0m'
red='\e[00;31m'
green='\e[00;32m'
yellow='\e[00;33m'
magenta='\e[00;35m'

# color shortcuts
d=${default}
r=${red}
g=${green}
y=${yellow}
m=${magenta}

# message indicators
info="${g}info:${d}   "
warn="${y}warn:${d}   "
error="${r}error:${d}  "

# runtime variables
disposableDependencies=()

# check user permissions
function checkPermissions {
  if [[ ${EUID} -ne 0 ]]; then
    echo -e "${error}This script must be run as ${m}root${d} or with ${m}sudo${d}."
    exit 1
  fi
}

# check if script is piped to bash
function checkPipe {
  if [ ! -t 0 ]; then
    echo -e "${error}This script can't be piped into bash."
    echo -e "${error}Please download the script and execute it afterwards."
    echo -e "${error}For further information refer to: ${m}https://gitlab.com/danproc/vm-scripts/${d}."
    exit 1
  fi
}

# join array with supplied delimiter
function joinArrayBy {
  local delimiter="${1}"
  shift
  echo -n "${1}"
  shift
  printf "%s" "${@/#/${delimiter}}"
}

# select package for removal
function selectPackage {
  echo -en "${info}Do you want to uninstall ${m}${1}${d}? [y/n] "
  read -r response
  if [[ "${response}" =~ ^([yY][eE][sS]|[yY])+$ ]]
  then
    disposableDependencies+=("${1}")
  fi
}

# uninstall application
function uninstallApplication {
  echo -e "${info}Unlinking ${m}/bin/${cmd}${d} ..."
  rm -rf "/bin/${cmd}" > /dev/null

  echo -e "${info}Removing source directory ${m}${srcDir}${d} ..."
  rm -rf "${srcDir}" > /dev/null

  echo -e "${info}Removing data directory ${m}${dataDir}${d} ..."
  rm -rf "${dataDir}" > /dev/null
}

# prompt for uninstallation of dependencies
function uninstallDependencies {
  echo -e "${info}You might want to uninstall some packages, that were installed along the application."
  echo -en "${info}Do you want to select the packages to uninstall now? [y/n] "
  read -r response
  if [[ ! "${response}" =~ ^([yY][eE][sS]|[yY])+$ ]]
  then
    echo -e "${info}Skipping uninstallation of dependencies!"
    exit 0
  fi
}

# display that uninstallation has succeeded
function displaySuccess {
  echo -e "${info}The application has successfully been uninstalled!"
  exit 0
}

# verify uninstallation of selected packages
function verifyUninstallation {
  echo -en "${warn}Are you sure that you want to remove these packages? [y/n] "
  read -r response
  if [[ ! "${response}" =~ ^([yY][eE][sS]|[yY])+$ ]]
  then
    echo -e "${info}Aborting uninstallation of dependencies!"
    displaySuccess
  else
    echo -e "${info}Uninstalling selected packages ..."
    apt-get remove $(joinArrayBy ' ' "${disposableDependencies[@]}") -y --autoremove
  fi
}

checkPipe
checkPermissions

uninstallApplication

uninstallDependencies

selectPackage git
selectPackage qemu-kvm
selectPackage libvirt-bin
selectPackage virtinst
selectPackage bridge-utils
selectPackage cloud-image-utils

verifyUninstallation

displaySuccess
