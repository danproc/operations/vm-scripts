#!/usr/bin/env bash

# command line alias
cmd="vms"

# application name
appName="vm-scripts"

# directories
srcBaseDir="/usr/src"
srcDir="${srcBaseDir}/${appName}"
dataBaseDir="/var/lib"
dataDir="${dataBaseDir}/${appName}"
imageDir="${dataDir}/images"
metaDir="${dataDir}/meta"
libvirtImageDir="/var/lib/libvirt/images"

# images
ubuntuXenialMirror="https://cloud-images.ubuntu.com/xenial/current"
ubuntuXenialImage="xenial-server-cloudimg-amd64-disk1"
ubuntuXenialChecksums="SHA256SUMS"
ubuntuXenialOsVariant="ubuntu16.04"

# global modules
source "${srcDir}/src/colors.bash"
source "${srcDir}/src/messages.bash"

case "${1}" in
  "list")
    source "${srcDir}/src/commands/list.bash"
    executeCommandList
    ;;
  "create")
    source "${srcDir}/src/commands/create.bash"
    executeCommandCreate
    ;;
  "delete")
    source "${srcDir}/src/commands/delete.bash"
    executeCommandDelete
    ;;
  "update")
    source "${srcDir}/src/commands/update.bash"
    executeCommandUpdate "$@"
    ;;
  "pull")
    source "${srcDir}/src/commands/pull.bash"
    executeCommandPull
    ;;
  "-v"|"--version")
    source "${srcDir}/src/options/version.bash"
    executeOptionVersion
    ;;
  "-h"|"--help"|"")
    source "${srcDir}/src/options/help.bash"
    executeOptionHelp
    ;;
  *)
    source "${srcDir}/src/options/help.bash"
    executeOptionHelp
    exit 1
    ;;
esac

exit 0
