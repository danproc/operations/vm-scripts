# Changelog

All notable changes to this project will be documented in this file.

This project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## 0.11.2 - 2018-08-24

### Bugfixes

- upgrade command in provisioning scripts also upgrades kernel version

## 0.11.1 - 2018-08-23

### Bugfixes

- revert master branch after attempting to update to Ubuntu 18.04 LTS Bionic Beaver

## 0.11.0 - 2018-08-12

### Notes

- repository changed location to be scoped in proper subgroup
- updating to this version requires running the uninstall script and then running the install script as shown in the [README.md](./README.md)

## 0.10.0 - 2018-08-11

### Features

- adds the **kubernetes_1.11** and the **haproxy_1.8** provisioning scripts
- virtual machines update all packages automatically at 3am local time every day

### Improvements

- remove old ssh host keys on VM creation

## 0.9.4 - 2018-08-09

### Bugfixes

- fix unnecessary tail command in version option implementation

### Improvements

- install script does not install recommended packages anymore

## 0.9.3 - 2018-08-08

### Bugfixes

- implement fix for version option to fetch version number automatically from changelog

## 0.9.2 - 2018-08-05

### Improvements

- **docker-ce_17.03.2** provisioning script also adds user _ubuntu_ to the _docker_ group

## 0.9.1 - 2018-08-03

### Bugfixes

- remove virt-install and qemu-img output

## 0.9.0 - 2018-08-03

### Bugfixes

- fixed severe bug where root filesystem would not be resized properly

### Notes

- updating to this version requires running the uninstall script and then running the install script as shown in the [README.md](./README.md)
- images are now using the raw `.img` format instead of `.qcow2`

## 0.8.0 - 2018-08-02

### Features

- command to list all existing virtual machines
- command to delete exisiting virtual machine

### Notes

- running the uninstall script and then running the **new** install script as shown in the [README.md](./README.md) is required to update to this version

## 0.7.0 - 2018-08-01

### Features

- possibility to select provisioning script when creating a new virtual machine

## 0.6.0 - 2018-08-01

### Features

- command to create new virtual machine

### Bugfixes

- fixed source import paths in scripts
- fixed version number

## 0.5.0 - 2018-08-01

### Features

- flag for update command to purge local changes

## 0.4.0 - 2018-07-30

### Features

- command to sync cloud image to disk

## 0.3.1 - 2018-07-29

### Bugfixes

- fix update command to update actual repository in installation directory

## 0.3.0 - 2018-07-28

### Features

- command to update script from git repository

## 0.2.0 - 2018-07-28

### Features

- option to display version

## 0.1.0 - 2018-07-28

### Features

- option to display help script
