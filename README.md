# VM scripts

This repository provides a tool that helps to commisson and manage virtual machines on a single host.

## Installation

Note, that this has only been tested on Ubuntu Xenial 16.04 LTS.

```bash
wget https://gitlab.com/danproc/operations/vm-scripts/raw/master/bin/install.bash -O /tmp/install.bash
sudo chmod +x /tmp/install.bash
sudo /tmp/install.bash
sudo rm /tmp/install.bash
```

## Usage

```txt
Usage:
  vms <command> [<options>]

Manage virtual machines via cloud-init and KVM.

Commands:
  list           list all existing virtual machines
  pull           pull latest cloud image
  create         create new virtual machine
  delete         delete existing virtual machine
  update         update this script from git repository
    -f, --force  force update by purging local repository

Options:
  -h, --help     display this help and exit
  -v, --version  display the current version
```

## Uninstallation

```bash
wget https://gitlab.com/danproc/operations/vm-scripts/raw/master/bin/uninstall.bash -O /tmp/uninstall.bash
sudo chmod +x /tmp/uninstall.bash
sudo /tmp/uninstall.bash
sudo rm /tmp/uninstall.bash
```

## License

This project is licensed under the terms of the [MIT license](https://mit-license.org/).
